//
//  Task.swift
//  Pods
//
//  Created by Nick Kibish on 1/5/17.
//
//

import UIKit

struct TaskCodingKeys {
    static let title = "Task.Title"
    static let completed = "Task.Completed"
    static let subtasks = "Task.Subtasks"
    static let rootObject = "Task.RootObject"
}

open class Task: NSObject, NSCoding, NSCopying {
    var _modifiedDate: Date
    var _subtasks = [Task]()
    var shouldUpdateSubtasks = true
    var shouldUpdateParrentTask = true
    
    public var subtasks: [Task] {
        return _subtasks
    }
    
    public var subtasksCount: Int {
        return _subtasks.count
    }
    
    public var allSubtasksCompleted: Bool {
        var completed = true
        subtasks.forEach({ completed = completed && $0.completed })
        return completed
    }
    
    public var modifiedDate: Date {
        return _modifiedDate
    }
    
    public dynamic var title: String {
        didSet {
            update(modifiedDate: Date())
        }
    }
    
    public dynamic var completed: Bool {
        didSet {
            update(modifiedDate: Date())
            if shouldUpdateSubtasks {
                subtasks.forEach { [unowned self] (task) in
                    task.completed = self.completed
                }
            }
            
            if shouldUpdateParrentTask {
                parrentTask?.changedCompletionInSubtask(updateSubtasks: shouldUpdateSubtasks)
            }
            shouldUpdateSubtasks = true
            shouldUpdateParrentTask = true
        }
    }
    
    public weak var parrentTask: Task?
    
    public init(title: String, completed: Bool = false) {
        self._modifiedDate = Date()
        self.title = title
        self.completed = completed
        super.init()
    }
    
    public required convenience init?(coder aDecoder: NSCoder) {
        guard let title = aDecoder.decodeObject(forKey: TaskCodingKeys.title) as? String else {
            return nil
        }
        
        let completed = aDecoder.decodeBool(forKey: TaskCodingKeys.completed)
        self.init(title: title, completed: completed)
        
        if let subtasks = aDecoder.decodeObject(forKey: TaskCodingKeys.subtasks) as? [Task] {
            _subtasks = subtasks
        }
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: TaskCodingKeys.title)
        aCoder.encode(subtasks, forKey: TaskCodingKeys.subtasks)
        aCoder.encode(completed, forKey: TaskCodingKeys.completed)
    }
    
    public func copy(with zone: NSZone? = nil) -> Any {
        let copyTask = Task(title: title, completed: completed)
        subtasks.forEach({ copyTask.append(subtask: $0.copy() as! Task) })
        return copyTask
    }
    
    open override func isEqual(_ object: Any?) -> Bool {
        guard let equalableTask = object as? Task else {
            return false
        }
        
        let titlesEqual = title == equalableTask.title
        let completedEqual = completed == equalableTask.completed
        
        if equalableTask.subtasksCount != subtasksCount {
            return false
        }
        
        return titlesEqual && completedEqual && compareSubtasks(with: equalableTask)
    }
    
    func compareSubtasks(with equalableTask: Task) -> Bool  {
        var subtasksEqual = true
        for task in subtasks {
            for eTask in equalableTask.subtasks {
                subtasksEqual = eTask.isEqual(task)
                if subtasksEqual {
                    break
                }
            }
            
            if !subtasksEqual {
                break
            }
        }
        
        return subtasksEqual
    }
}

//MARK: - Archive Methods
extension Task {
    
    /// Save list of tasks to userDefaults
    ///
    /// - Parameters:
    ///   - tasks: list of tasks
    ///   - userDefaults: current User Defaults (recomend use UserDefaults.standard)
    public class func save(tasks: [Task], to userDefaults: UserDefaults = UserDefaults.standard) {
        let data = NSKeyedArchiver.archivedData(withRootObject: tasks)
        userDefaults.set(data, forKey: TaskCodingKeys.rootObject)
        userDefaults.synchronize()
    }
    
    /// Load tasks from user defaults
    ///
    /// - Parameter userDefaults: current user defaults (recomend use default value)
    /// - Returns: list of tasks
    public class func loadTasks(from userDefaults: UserDefaults = UserDefaults.standard) -> [Task] {
        if let taskData = userDefaults.object(forKey: TaskCodingKeys.rootObject) as? Data {
            if let tasks = NSKeyedUnarchiver.unarchiveObject(with: taskData) as? [Task] {
                return tasks
            }
        }
        return []
    }
}

//MARK: - Methods
extension Task {
    public func append(subtask: Task) {
        subtask.parrentTask = self
        _subtasks.append(subtask)
        subtask.update(modifiedDate: Date())
        changedCompletionInSubtask(updateSubtasks: false)
    }
    
    public func remove(subtask: Task) {
        if let index = _subtasks.index(of: subtask) {
            subtask.update(modifiedDate: Date())
            subtask.parrentTask = nil
            _subtasks.remove(at: index)
            changedCompletionInSubtask(updateSubtasks: false)
        }
    }
    
    public func removeAllSubtasks() {
        let modDate = Date()
        _subtasks.forEach { (task) in
            task._modifiedDate = modDate
            task.parrentTask = nil 
        }
        _subtasks.removeAll()
        _modifiedDate = modDate
    }
    
    public func markAllSubtasksAsCompleted() {
        subtasks.forEach({ $0.completed = true })
        completed = true
    }
}

//MARK: - Private Methods
extension Task {
    internal func setCompletedWithoutUpdatingSubtasks(completed: Bool) {
        shouldUpdateSubtasks = false
        self.completed = completed
    }
    
    internal func changedCompletionInSubtask(updateSubtasks: Bool) {
        shouldUpdateSubtasks = updateSubtasks
        var completed = true
        
        subtasks.forEach { (task) in
            completed = completed && task.completed
        }
        
        if completed != self.completed {
            self.setCompletedWithoutUpdatingSubtasks(completed: completed)
            parrentTask?.changedCompletionInSubtask(updateSubtasks: updateSubtasks)
        }
    }
    
    internal func update(modifiedDate: Date) {
        _modifiedDate = modifiedDate
        parrentTask?.update(modifiedDate: modifiedDate)
    }
}
