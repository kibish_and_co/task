//
//  TaskSpec.swift
//  Task
//
//  Created by Nick Kibish on 1/6/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import Quick
import Nimble
import Task

class TaskSpec: QuickSpec {
    override func spec() {
        var parrentTask, childTask1, childTask2: Task!
        beforeEach {
            parrentTask = Task(title: "Parrent Task")
            childTask1 = Task(title: "Child Task 1")
            childTask2 = Task(title: "Child Task 2")
        }
        
        describe("Test Methods") {
            context("added child task") {
                it("set child task") {
                    parrentTask.append(subtask: childTask1)
                    parrentTask.append(subtask: childTask2)
                    
                    expect(parrentTask.subtasksCount) == 2
                    expect(childTask1.parrentTask) == parrentTask
                }
            }
        }
    }
}
