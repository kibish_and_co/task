//
//  TestTask.swift
//  Task
//
//  Created by Nick Kibish on 1/6/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import XCTest
import Task

class TestTask: XCTestCase {
    var parrentTask: Task!
    var childTask1: Task!
    var childTask2: Task!
    
    var userDefaults: UserDefaults!
    let userDefaultsSuiteName = "TestDefaults"
    
    override func setUp() {
        super.setUp()
        
        UserDefaults().removePersistentDomain(forName: userDefaultsSuiteName)
        userDefaults = UserDefaults(suiteName: userDefaultsSuiteName)
        
        parrentTask = Task(title: "New Parrent Task")
        childTask1 = Task(title: "Child Task 1")
        childTask2 = Task(title: "Child Task 2")
    }
    
    func testAppend() {
        reappendSubtasks()
        
        XCTAssertEqual(parrentTask.subtasksCount, 2)
        XCTAssertEqual(childTask2.parrentTask, parrentTask)
        XCTAssertEqual(parrentTask.subtasks.first, childTask1)
    }
    
    func testRemove() {
        reappendSubtasks()
        
        parrentTask.remove(subtask: childTask1)
        
        XCTAssertEqual(parrentTask.subtasksCount, 1)
        XCTAssertNil(childTask1.parrentTask)
        
        parrentTask.removeAllSubtasks()
        XCTAssertEqual(parrentTask.subtasksCount, 0)
        XCTAssertNil(childTask2.parrentTask)
    }
    
    /// test modification date
    func testModification() {
        reappendSubtasks()
        
        var modDate = parrentTask.modifiedDate.timeIntervalSince1970
        var childModDate = childTask2.modifiedDate.timeIntervalSince1970
        
        XCTAssertEqual(modDate, childModDate)
        
        parrentTask.markAllSubtasksAsCompleted()
        
        modDate = parrentTask.modifiedDate.timeIntervalSince1970
        childModDate = childTask2.modifiedDate.timeIntervalSince1970
        XCTAssertEqual(modDate, childModDate)
        
        childTask1.completed = false
        childTask2.completed = false
        
        modDate = parrentTask.modifiedDate.timeIntervalSince1970
        childModDate = childTask2.modifiedDate.timeIntervalSince1970
        XCTAssertEqual(modDate, childModDate)
    }
    
    /// Test changing completion
    func testCompletionSubtasks() {
        let parrentTask = Task(title: "Mock Parrent Task")
        let childTask1 = Task(title: "Mock Child Task 1")
        let childTask2 = Task(title: "Mock Child Task 2")
        
        parrentTask.append(subtask: childTask1)
        parrentTask.append(subtask: childTask2)
        
        XCTAssertFalse(parrentTask.completed)
        
        parrentTask.markAllSubtasksAsCompleted()
        
        XCTAssertTrue(childTask1.completed)
        XCTAssertTrue(childTask2.completed)
        XCTAssertTrue(parrentTask.completed)
        
        childTask1.completed = false
        XCTAssertFalse(parrentTask.completed)
        XCTAssertTrue(childTask2.completed)
        
        childTask1.completed = true
        XCTAssertTrue(childTask1.completed)
        XCTAssertTrue(childTask2.completed)
        XCTAssertTrue(parrentTask.completed)
        
        let subSubTask = Task(title: "Sub-Sub-Task")
        childTask1.append(subtask: subSubTask)
        
        XCTAssertFalse(childTask1.completed)
        XCTAssertFalse(parrentTask.completed)
        
        subSubTask.completed = true
        XCTAssertTrue(childTask1.completed)
        XCTAssertTrue(parrentTask.completed)
        
        subSubTask.completed = false
        XCTAssertFalse(childTask1.completed)
        XCTAssertFalse(parrentTask.completed)
        
        childTask2.completed = false
        childTask1.completed = true
        XCTAssertTrue(subSubTask.completed)
        XCTAssertFalse(parrentTask.completed)
        
        let subSubTask2 = Task(title: "Another Sub-Sub-Task")
        childTask1.append(subtask: subSubTask2)
        subSubTask.completed = false
        
        subSubTask.completed = true
        
        XCTAssertFalse(childTask1.completed)
        XCTAssertFalse(subSubTask2.completed)
        XCTAssertTrue(subSubTask.completed)
    }
    
    func testTaskArchiving() {
        reappendSubtasks()
        childTask1.removeAllSubtasks()
        childTask2.removeAllSubtasks()
        
        childTask1.completed = true
        
        let subSubTask = Task(title: "Sub-Sub-Task")
        childTask2.append(subtask: subSubTask)
        
        Task.save(tasks: [parrentTask], to: userDefaults)
        
        let tasks = Task.loadTasks(from: userDefaults)
        XCTAssertEqual(tasks.count, 1)
        
        let pTask = tasks[0]
        XCTAssertEqual(pTask.subtasks.count, 2)
        XCTAssertTrue(pTask.subtasks[0].completed)
        XCTAssertEqual(pTask.subtasks[1].subtasks.count, 1)
    }
    
    func testCopyingAndEqual() {
        reappendSubtasks()
        
        let newParrentTask = parrentTask.copy() as! Task
        XCTAssertEqual(newParrentTask.subtasksCount, 2)
        
        XCTAssertEqual(parrentTask, newParrentTask)
    }
    
    func reappendSubtasks() {
        parrentTask.removeAllSubtasks()
        parrentTask.append(subtask: childTask1)
        parrentTask.append(subtask: childTask2)
    }
    
    override func tearDown() {
        UserDefaults().removeSuite(named: userDefaultsSuiteName)
        
        super.tearDown()
    }
}
