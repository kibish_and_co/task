//
//  ChildTaskListViewController.swift
//  Task
//
//  Created by Nick Kibish on 1/8/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import UIKit
import Task

class ChildTaskListViewController: TaskListTableViewController {
    var parrentTask: Task! {
        didSet {
            parrentTask.subtasks.forEach { (task) in
                task.addObserver(self, forKeyPath: "title", options: .new, context: &myContext)
                task.addObserver(self, forKeyPath: "completed", options: .new, context: &myContext)
            }
        }
    }
    
    override var dataSource: [Task] {
        return parrentTask.subtasks
    }
    
    class func storyboardInstance() -> ChildTaskListViewController {
        let storyboard = UIStoryboard(name: "TaskList", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ChildTaskListViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = parrentTask.title
    }
    
    
    
    /// leave thim method empty to avoid wrong saving
    override func save() { }
}

extension ChildTaskListViewController {
    override func add(task: Task) {
        parrentTask.append(subtask: task)
    }
    
    override func delete(task: Task) {
        task.removeObserver(self, forKeyPath: "completed")
        task.removeObserver(self, forKeyPath: "title")
        
        parrentTask.remove(subtask: task)
    }
}
