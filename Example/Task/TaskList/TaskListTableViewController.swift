//
//  TaskListTableViewController.swift
//  Task
//
//  Created by Nick Kibish on 1/8/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import UIKit
import Task

internal var myContext = 0

/// table view controller with root tasks
class TaskListTableViewController: UITableViewController {
    
    /// saved tasks
    lazy var _dataSource: [Task] = {
        let tasks = Task.loadTasks(from: UserDefaults.standard)
        
        tasks.forEach({ (task) in
            task.addObserver(self, forKeyPath: "title", options: .new, context: &myContext)
            task.addObserver(self, forKeyPath: "completed", options: .new, context: &myContext)
        })
        return tasks
    }()
    
    /// Calculated var for list of tasks
    var dataSource: [Task] {
        return _dataSource
    }
    
    /// Create actions for actionSheet
    ///
    /// - Returns: array of actions
    func createActions() -> [UIAlertAction] {
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        return [addTaskAction(), cancelAction]
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &myContext {
            if let task = object as? Task {
                update(task: task)
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    /// Find cell related to task and update it title
    /// If task completed, the title is Striked Through
    ///
    /// - Parameter task: task, which was updated
    func update(task: Task) {
        guard let index = dataSource.index(of: task) else {
            return
        }
        
        let attrString = NSMutableAttributedString(string: task.title)
        if task.completed {
            attrString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, task.title.characters.count))
        }
        
        let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0))
        cell?.textLabel?.attributedText = attrString
        
        save()
    }
    
    func save() {
        Task.save(tasks: dataSource, to: UserDefaults.standard)
    }
}

//MARK: - Table View Delegate
extension TaskListTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let task = dataSource[indexPath.row]
        
        let childVC = ChildTaskListViewController.storyboardInstance()
        childVC.parrentTask = task
        navigationController?.pushViewController(childVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let task = dataSource[indexPath.row]
        
        let attrString = NSMutableAttributedString(string: task.title)
        if task.completed {
            attrString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, task.title.characters.count))
        }
        
        cell.textLabel?.attributedText = attrString
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) { }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = deleteAction()
        
        let complete = completeAction()
        complete.backgroundColor = UIColor(red: 79.0/255, green: 248.0/255, blue: 140.0/255, alpha: 1)
        
        let edit = editTaskAction()
        edit.backgroundColor = UIColor(red: 79.0/255, green: 140.0/255, blue: 248.0/255, alpha: 1)
        
        return [delete, complete, edit]
    }
    
    /// Aciton for deleting task
    ///
    /// - Returns: UITableViewRowAction
    func deleteAction() -> UITableViewRowAction {
        return UITableViewRowAction(style: .destructive, title: "Delete") { [unowned self] (_, indexPath) in
            let task = self.dataSource[indexPath.row]
            self.delete(task: task)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    /// creates action for editing task
    ///
    /// - Returns: UITableViewRowAction
    func editTaskAction() -> UITableViewRowAction {
        return UITableViewRowAction(style: .normal, title: "Edit") { [unowned self] (_, indexPath) in
            let task = self._dataSource[indexPath.row]
            
            let editTaskAlert = UIAlertController(title: "Add Task", message: nil, preferredStyle: .alert)
            editTaskAlert.addTextField(configurationHandler: nil)
            guard let editTaskTitle = editTaskAlert.textFields?.first else {
                return
            }
            editTaskTitle.text = task.title
            editTaskTitle.placeholder = "Task"
            
            let confirmAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                guard let title = editTaskTitle.text else {
                    task.title = editTaskTitle.placeholder!
                    return
                }
                task.title = title.characters.count > 0 ? title : editTaskTitle.placeholder!
            })
            
            let rejectAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            editTaskAlert.addAction(confirmAction)
            editTaskAlert.addAction(rejectAction)
            self.present(editTaskAlert, animated: true, completion: nil)
            self.tableView.setEditing(false, animated: true)
        }
    }
    
    /// Creates action for completion task
    ///
    /// - Returns: UITableViewRowAction
    func completeAction() -> UITableViewRowAction {
        return UITableViewRowAction(style: .normal, title: "Complete", handler: { [unowned self] (_, indexPath) in
            let task = self.dataSource[indexPath.row]
            task.completed = task.completed ? false : true 
            self.tableView.setEditing(false, animated: true)
        })
    }
}

//MARK: - Action Sheet Actions
extension TaskListTableViewController {
    
    /// Create new task
    ///
    /// - Returns: action for actionSheet
    func addTaskAction() -> UIAlertAction {
        return UIAlertAction(title: "Add Task", style: .default) { [unowned self] (action) in
            let newTaskAlert = UIAlertController(title: "Add Task", message: nil, preferredStyle: .alert)
            newTaskAlert.addTextField(configurationHandler: nil)
            
            guard let editTaskTitle = newTaskAlert.textFields?.first else {
                return
            }
            editTaskTitle.placeholder = "Task"
            
            let confirmAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                guard let newTaskTitle = editTaskTitle.text else {
                    return
                }
                let task = Task(title: newTaskTitle.characters.count > 0 ? newTaskTitle : editTaskTitle.placeholder!)
                task.addObserver(self, forKeyPath: "title", options: .new, context: &myContext)
                task.addObserver(self, forKeyPath: "completed", options: .new, context: &myContext)
                self.add(task: task)
                
                self.tableView.insertRows(at: [IndexPath(row: self.dataSource.count - 1, section: 0)], with: .automatic)
            })
            
            let rejectAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            newTaskAlert.addAction(confirmAction)
            newTaskAlert.addAction(rejectAction)
            self.present(newTaskAlert, animated: true, completion: nil)
        }
    }
    
    /// Add new task to data source
    ///
    /// - Parameter task: task to add
    func add(task: Task) {
        _dataSource.append(task)
        Task.save(tasks: self.dataSource, to: UserDefaults.standard)
    }
    
    func delete(task: Task) {
        guard let index = _dataSource.index(of: task) else {
            return
        }
        
        task.removeObserver(self, forKeyPath: "completed")
        task.removeObserver(self, forKeyPath: "title")
        
        _dataSource.remove(at: index)
    }
}

//MARK: - Actions
extension TaskListTableViewController {
    
    /// Openes actions for tasks
    ///
    /// - Parameter sender: barButtonItem
    @IBAction func taskAction(_ sender: Any) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for action in createActions() {
            actionSheet.addAction(action)
        }
        
        present(actionSheet, animated: true, completion: nil)
    }
}
