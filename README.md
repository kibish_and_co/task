# Task

[![CI Status](http://img.shields.io/travis/Nick Kibish/Task.svg?style=flat)](https://travis-ci.org/Nick Kibish/Task)
[![Version](https://img.shields.io/cocoapods/v/Task.svg?style=flat)](http://cocoapods.org/pods/Task)
[![License](https://img.shields.io/cocoapods/l/Task.svg?style=flat)](http://cocoapods.org/pods/Task)
[![Platform](https://img.shields.io/cocoapods/p/Task.svg?style=flat)](http://cocoapods.org/pods/Task)

## Description
Проект выполнен в виде сторонней библиотеки. Сам класс `Task` можно подключить к другому проекту с помощью `CocoaPods`. 

### Логика работы
Сам класс имеет следующую логику:

* ModificationDate изменяется если поменять `completed` или `title` у задачи, либо это же свойства у любой подзадачи
* Если свойство `completed` изменить у родительской задачи - подзадачи автоматически изменят свое состояние
* Если completed изменить у подзадачи - родительская задача проверит все подзадачи, и если все подзадачи будут иметь статус `выполнено`, то родительская задача тоже станет выполненой. Если хотя бы одна подзадача будет невыполненой - родительская задача тоже останется невыполненной.
* Свойства `completed` и `title` являются `dynamic` и могут отслеживаться с применением KVO

## Example

Был также разработан тестовый проект.
Работа пока нестабильная, но можно посмотреть основные методы класса.

![alt text](https://bytebucket.org/kibish_and_co/task/raw/ba48f6cd5e58d019039b77dcfbe74ea52247b7e9/screenshots/Simulator%20Screen%20Shot%20Jan%2013%2C%202017%2C%2001.08.32.png?token=1785a8ae2ea1294b0f67bf3197ac6acca7250754 "screenshots")
![alt text](https://bytebucket.org/kibish_and_co/task/raw/ba48f6cd5e58d019039b77dcfbe74ea52247b7e9/screenshots/Simulator%20Screen%20Shot%20Jan%2013%2C%202017%2C%2001.08.35.png?token=49e3b4ca7a01b43be7e2e842d32175d4ec123d71 "screenshots")
![alt text](https://bytebucket.org/kibish_and_co/task/raw/ba48f6cd5e58d019039b77dcfbe74ea52247b7e9/screenshots/Simulator%20Screen%20Shot%20Jan%2013%2C%202017%2C%2001.08.40.png?token=d0f4f4342756c4284cc4651152e904273f37c2b9 "screenshots")
![alt text](https://bytebucket.org/kibish_and_co/task/raw/ba48f6cd5e58d019039b77dcfbe74ea52247b7e9/screenshots/Simulator%20Screen%20Shot%20Jan%2013%2C%202017%2C%2001.08.46.png?token=41f6b3021c299739317af1329f3c0aa9e6c1c537 "screenshots")

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Task is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Task"
```

## Author

Nick Kibish, nick.kibish@gmail.com

## License

Task is available under the MIT license. See the LICENSE file for more info.
