#
# Be sure to run `pod lib lint Task.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Task'
  s.version          = '1.0.0'
  s.summary          = 'Test Project for OWOX'

  s.description      = <<-DESC
This is test project from OWOX company
                       DESC

  s.homepage         = 'https://bitbucket.org/kibish_and_co/task'
  s.screenshots     = 'https://bytebucket.org/kibish_and_co/task/raw/ba48f6cd5e58d019039b77dcfbe74ea52247b7e9/screenshots/Simulator%20Screen%20Shot%20Jan%2013%2C%202017%2C%2001.08.32.png?token=1785a8ae2ea1294b0f67bf3197ac6acca7250754', 'https://bytebucket.org/kibish_and_co/task/raw/ba48f6cd5e58d019039b77dcfbe74ea52247b7e9/screenshots/Simulator%20Screen%20Shot%20Jan%2013%2C%202017%2C%2001.08.35.png?token=49e3b4ca7a01b43be7e2e842d32175d4ec123d71', 'https://bytebucket.org/kibish_and_co/task/raw/ba48f6cd5e58d019039b77dcfbe74ea52247b7e9/screenshots/Simulator%20Screen%20Shot%20Jan%2013%2C%202017%2C%2001.08.40.png?token=d0f4f4342756c4284cc4651152e904273f37c2b9', 'https://bytebucket.org/kibish_and_co/task/raw/ba48f6cd5e58d019039b77dcfbe74ea52247b7e9/screenshots/Simulator%20Screen%20Shot%20Jan%2013%2C%202017%2C%2001.08.46.png?token=41f6b3021c299739317af1329f3c0aa9e6c1c537'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Nick Kibish' => 'nick.kibish@gmail.com' }
  s.source           = { :git => 'https://NickKibish@bitbucket.org/kibish_and_co/task.git', :tag => s.version.to_s }
  

  s.ios.deployment_target = '8.0'

  s.source_files = 'Task/Classes/**/*'
end
